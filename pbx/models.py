# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.utils import timezone
from django.db import models


class CallCost(models.Model):
    area_code_pattern = models.CharField(primary_key=True, max_length=6)
    tier = models.IntegerField()
    cost = models.DecimalField(max_digits=7, decimal_places=2)
    seconds = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'call_cost'
        unique_together = (('area_code_pattern', 'tier'),)


class Calls(models.Model):
    call_id = models.AutoField(primary_key=True)
    call_type = models.CharField(max_length=2, blank=True, null=True)
    begin_date = models.DateField(blank=True, null=True)
    begin_datetime = models.DateTimeField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    end_datetime = models.DateTimeField(blank=True, null=True)
    duration_seconds = models.IntegerField(blank=True, null=True)
    duration_minutes = models.IntegerField(blank=True, null=True)
    station = models.CharField(max_length=10, blank=True, null=True)
    trunk_line = models.SmallIntegerField(blank=True, null=True)
    number_dialed = models.CharField(max_length=20, blank=True, null=True)
    area_code_dialed = models.CharField(max_length=3, blank=True, null=True)
    caller_id = models.CharField(max_length=20, blank=True, null=True)
    account_number = models.CharField(max_length=16, blank=True, null=True)
    dialed_name = models.CharField(max_length=150, blank=True, null=True)
    dialed_address = models.CharField(max_length=100, blank=True, null=True)
    dialed_city = models.CharField(max_length=100, blank=True, null=True)
    dialed_state = models.CharField(max_length=2, blank=True, null=True)
    dialed_zip = models.CharField(max_length=10, blank=True, null=True)
    cost = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'calls'


class GroupCostPeriod(models.Model):
    group_id = models.IntegerField()
    cost_period_name = models.CharField(max_length=50, blank=True, null=True)
    cost_begin_datetime = models.DateTimeField()
    cost_end_datetime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_cost_period'


class GroupStationMap(models.Model):
    group_id = models.IntegerField(primary_key=True)
    station = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'group_station_map'
        unique_together = (('group_id', 'station'),)


class Setting(models.Model):
    name = models.CharField(max_length=25)
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'setting'


class SmdrData(models.Model):
    id = models.IntegerField(primary_key=True)
    call_type = models.CharField(max_length=5, blank=True, null=True)
    input_time = models.CharField(max_length=5, blank=True, null=True)
    input_date = models.CharField(max_length=5, blank=True, null=True)
    trunk_line = models.CharField(max_length=4, blank=True, null=True)
    duration = models.TimeField(blank=True, null=True)
    station = models.CharField(max_length=10, blank=True, null=True)
    number_dialed = models.CharField(max_length=20, blank=True, null=True)
    account_number = models.CharField(max_length=16, blank=True, null=True)
    received_datetime = models.DateTimeField()
    received_date = models.DateField()
    area_code_dialed = models.CharField(max_length=3, blank=True, null=True)
    call_init_datetime = models.DateTimeField()
    call_end_datetime = models.DateTimeField()
    smdr_call_type = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'smdr_data'


class SmdrGroups(models.Model):
    group_id = models.AutoField(primary_key=True)
    group_name = models.CharField(max_length=50)
    group_desc = models.CharField(max_length=250, blank=True, null=True)
    everyone = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'smdr_groups'


class SmdrLog(models.Model):
    call_type = models.CharField(max_length=5, blank=True, null=True)
    input_time = models.CharField(max_length=10, blank=True, null=True)
    input_date = models.CharField(max_length=10, blank=True, null=True)
    trunk_line = models.CharField(max_length=4, blank=True, null=True)
    duration = models.CharField(max_length=8, blank=True, null=True)
    station = models.CharField(max_length=10, blank=True, null=True)
    number_dialed = models.CharField(max_length=20, blank=True, null=True)
    account_number = models.CharField(max_length=16, blank=True, null=True)
    received_datetime = models.DateTimeField()
    received_data = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'smdr_log'


class SmdrStations(models.Model):
    station = models.CharField(primary_key=True, max_length=10)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'smdr_stations'

class owner_report(models.Model):
    owner = models.CharField(max_length = 30)
    station = models.CharField(max_length = 10)
    from_t = models.DateTimeField(default = timezone.now)
    to_t = models.DateTimeField(default = timezone.now)
    class Meta:
        db_table="owner_report"

class owner(models.Model):
    owner = models.CharField(max_length = 30,unique = True)
    station = models.CharField(max_length = 10, unique = True)
    from_t = models.DateTimeField(default = timezone.now)
    to_t = models.DateTimeField(blank = True,null = True)
    def save(self, *args, **kwargs):
        if self.to_t!= None:
            obj = owner_report(owner = self.owner,station=self.station,from_t = self.from_t,to_t=self.to_t)
            obj.save()
        models.Model.save(self, *args,**kwargs)
    class Meta:
        db_table="owner"


class SmdrUsers(models.Model):
    username = models.CharField(primary_key=True, max_length=30)
    password = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'smdr_users'
