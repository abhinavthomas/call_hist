from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.http import HttpResponse,HttpResponseRedirect
from models import *
# Create your views here.

def index(request):
    return render(request,'index.html')

@csrf_protect
def login_user(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/home/')
    if request.method == 'GET':
        return render(request,'login.html',{})
    if request.method ==  'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                return HttpResponseRedirect('/home/')
            else :
                raise Exception
        except Exception as e:
            return render(request,'login.html',{'obj':'Invalid user name or password'})
@csrf_protect
def report(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            return render(request,'date.html',{'page':'report'})
        else:
            return render(request,'login.html',{'obj':'Please login first'})

    elif request.method == 'POST':
        str_mon = request.POST.get('str_mon')+" 00:00"
        end_mon = request.POST.get('end_mon')+" 00:00"
        print str_mon,end_mon      
        if request.user.is_authenticated():
            obj = SmdrLog.objects.filter(received_datetime__range =[str_mon,end_mon])
            return render(request,'report.html',{'username':request.user,'obj':obj})
        else:
            return render(request,'login.html',{'obj':'Please login first'})

def summary(request):
    if request.user.is_authenticated():
        l = owner.objects.all()
        obj = [(i.owner,i.station,SmdrLog.objects.filter(number_dialed=None).filter(station = i.station).count(),SmdrLog.objects.exclude(number_dialed = None).filter(station = i.station).count(),SmdrLog.objects.exclude(duration=None).filter(station=i.station).count(),SmdrLog.objects.filter(station = i.station).filter(duration=None).count()) for i in l]
        return render(request,'summary.html',{'username':request.user,'obj':obj})
    else:
        return render(request,'login.html',{'obj':'Please login first'})

def station(request):
    if request.user.is_authenticated():
        if str(request.user) == 'admin':
            obj = owner_report.objects.all()
            return render(request,'sr.html',{'username':str(request.user),'obj':obj})
        else:
            return render(request,'login.html',{'obj':"Ooopss!! You don't have the permission to access this page."})
    else:
        return render(request,'login.html',{'obj':'Please login first'})

def home(request):
    if request.user.is_authenticated():
        return render(request,'home.html',{'username':str(request.user)})
    else:
        return render(request,'login.html',{'obj':'Please login first'})

def logout_user(request):
    logout(request)
    return render(request,'login.html',{'obj':'Logged out successfully'})
